package de.hsb.e;

public class E {

    private final F f;

    public E(F f) {
        this.f = f;
    }

    public boolean complexCode(int a) {
        /*
        some Code, which is even more complex
         */
        return f.extremComplextCode(a);
    }
}
