package de.hsb.a;

import java.util.Collection;

public class A {

    public int sum(Collection<Integer> numbers) {
        int result = 0;
        for(Integer i : numbers) {
            result += i;
        }
        return result;
    }
}
