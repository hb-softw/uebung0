package de.hsb.b;

/*
    Es wird ein Integer gegeben und wenn dieser durch 2 teilbar ist,
    soll true zurückgegeben werden, anderenfalls false
 */
public class B {

    public boolean b(int number) {
        return number % 2 == 0;
    }
}
