package de.hsb.c;

/*
Soll true zurückgeben, wenn der Parameter mindestens 2 ist.
 */
public class C {

    public boolean a(int a) {
        return a > 2;
    }
}
