package de.hsb.d;

/*
Soll true zurückgeben, wenn die Eingabezahl durch 3 teilbar ist.
 */
public class D {

    public boolean code(int i) {
        return i % 2 == 0;
    }
}
