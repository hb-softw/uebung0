package de.hsb.d;

import de.hsb.c.C;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DTest {

    D uut;

    @BeforeEach
    public void setUp() {
        uut = new D();
    }

    @ParameterizedTest
    @ValueSource(ints = {0,2,4,6,100,30542,145646})
    void returnsTrueWhenInputMod2Is0(int input) {
        assertThat(uut.code(input))
                .isEqualTo(true);
    }

    @ParameterizedTest
    @ValueSource(ints = {1,3,5,7,9564665,4655641,231,6541})
    void returnsFalseWhenInputMod2Not0(int input) {
        assertThat(uut.code(input))
                .isEqualTo(false);
    }
}