package de.hsb.c;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CTest {

    C uut;

    @BeforeEach
    public void setUp() {
        uut = new C();
    }

    @Test
    void returnsFalseWhenSmallerThan2() {
        assertThat(uut.a(1))
                .isEqualTo(false);
    }

    @Test
    void returnsTrueWhenBiggerThan2() {
        assertThat(uut.a(3))
                .isEqualTo(true);
    }
}