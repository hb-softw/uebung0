package de.hsb.e;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ETest {

    @Mock
    private F mock;

    @InjectMocks
    private E uut;


    @Test
    void test() {
        when(mock.extremComplextCode(1))
                .thenReturn(true);

        boolean result = uut.complexCode(1);

        assertThat(result)
                .isEqualTo(true);
    }

}