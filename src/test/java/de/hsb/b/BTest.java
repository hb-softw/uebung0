package de.hsb.b;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


class BTest {

    B uut;


    @BeforeEach
    public void setUp() {
        uut = new B();
    }

    @Test
    void returnsTrueWhenDividableBy2() {
        boolean result = uut.b(4);

        assertThat(result)
                .isEqualTo(true);
    }
}